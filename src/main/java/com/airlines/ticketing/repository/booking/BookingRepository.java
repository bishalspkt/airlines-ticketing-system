package com.airlines.ticketing.repository.booking;

import com.airlines.ticketing.model.Booking;
import com.airlines.ticketing.model.RegisteredUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by bishal on 9/9/17.
 */

@Repository
@Transactional
public interface BookingRepository extends CrudRepository<Booking, Integer>{


    public List<Booking> findAllByRegisteredUser(RegisteredUser user);
}
