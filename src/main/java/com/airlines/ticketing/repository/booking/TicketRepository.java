package com.airlines.ticketing.repository.booking;

import org.springframework.data.repository.CrudRepository;
import com.airlines.ticketing.model.Ticket;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bishal on 9/10/17.
 */

@Repository
@Transactional
public interface TicketRepository extends CrudRepository<Ticket, Integer>{
}
