package com.airlines.ticketing.repository.booking;

import com.airlines.ticketing.model.Passenger;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bishal on 9/10/17.
 */

@Repository
@Transactional
public interface PassengerRepository extends CrudRepository<Passenger,Integer>{
}
