package com.airlines.ticketing.repository.user;

import com.airlines.ticketing.model.RegisteredUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by bishal on 9/10/17.
 */

@Repository
@Transactional
public interface RegisteredUserRepository extends CrudRepository<RegisteredUser, Integer>{

    RegisteredUser findByEmail(String email);
    List<RegisteredUser> findAllByEmail(String email);

}
