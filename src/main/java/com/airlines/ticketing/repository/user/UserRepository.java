package com.airlines.ticketing.repository.user;

import com.airlines.ticketing.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by bishal on 9/10/17.
 */

@Repository
@Transactional
public interface UserRepository extends CrudRepository<User, Integer>{
}
