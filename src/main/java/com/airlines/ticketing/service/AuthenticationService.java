package com.airlines.ticketing.service;

import com.airlines.ticketing.model.RegisteredUser;
import com.airlines.ticketing.model.User;
import com.airlines.ticketing.repository.user.RegisteredUserRepository;
import com.airlines.ticketing.repository.user.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by bishal on 9/10/17.
 */
@Service
public class AuthenticationService {

    private final RegisteredUserRepository userRepository;

    public AuthenticationService(RegisteredUserRepository userRepository){
        this.userRepository = userRepository;
    }

    /* Populates dummy record */
    public void addUser(){
        userRepository.save(new RegisteredUser("Bishal","user@example.com","123"));
    }

    public boolean authenticate(String email, String password){
        List<RegisteredUser> users = userRepository.findAllByEmail(email);
        if (users.isEmpty())
            return false;

        return users.get(0).getPassword().equals(password);
    }

    public String getName(String email){
        List<RegisteredUser> users = userRepository.findAllByEmail(email);
        if (users.isEmpty())
            return null;
        return users.get(0).getName();

    }

    public Iterable<RegisteredUser> findAll(){
        return userRepository.findAll();
    }
}
