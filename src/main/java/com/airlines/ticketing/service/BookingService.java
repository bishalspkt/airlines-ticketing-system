package com.airlines.ticketing.service;

import com.airlines.ticketing.data.booking.BookingData;
import com.airlines.ticketing.data.flight.FlightData;
import com.airlines.ticketing.data.passenger.FClass;
import com.airlines.ticketing.data.passenger.Luggage;
import com.airlines.ticketing.data.passenger.Meal;
import com.airlines.ticketing.data.passenger.PassengerData;
import com.airlines.ticketing.model.Booking;
import com.airlines.ticketing.model.Passenger;
import com.airlines.ticketing.model.RegisteredUser;
import com.airlines.ticketing.model.Ticket;
import com.airlines.ticketing.repository.booking.BookingRepository;
import com.airlines.ticketing.repository.booking.PassengerRepository;
import com.airlines.ticketing.repository.booking.TicketRepository;
import com.airlines.ticketing.repository.user.RegisteredUserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bishal on 9/10/17.
 */
@Service
public class BookingService {

    private final TicketRepository ticketRepository;

    private final BookingRepository bookingRepository;

    private final PassengerRepository passengerRepository;

    private final RegisteredUserRepository registeredUserRepository;

    public BookingService(TicketRepository ticketRepository,
                          BookingRepository bookingRepository,
                          PassengerRepository passengerRepository,
                          RegisteredUserRepository registeredUserRepository ) {
        this.ticketRepository = ticketRepository;
        this.bookingRepository = bookingRepository;
        this.passengerRepository = passengerRepository;
        this.registeredUserRepository = registeredUserRepository;
    }

    public BookingData AddBooking(PassengerData passengerData, FlightData flightData,
                                  String name, String email, String seat, double price)
    {
        RegisteredUser user = registeredUserRepository.findByEmail(email);
        System.out.println("BookiingService - The corresponding user for "+ email + " is " + user);
        Booking booking = new Booking(user, flightData.getFromCity(),
                flightData.getToCity(),
                flightData.getDate(),
                flightData.getDeparture(),
                flightData.getArrival());


        Booking modelBooking = bookingRepository.save(booking);


        Passenger passenger = new Passenger(passengerData.getName(), passengerData.getDob());
        passengerRepository.save(passenger);
        Ticket ticket = new Ticket(booking, passenger, passengerData.getMeal().toString(),
                passengerData.getLuggage().toString(), passengerData.getFClass().toString(),
                seat, flightData.getName(), price);
        ticketRepository.save(ticket);

        return new BookingData(modelBooking.getId());
    }

    public List<BookingData> getAllBooking(String email){
        RegisteredUser user = registeredUserRepository.findByEmail(email);
        if (user == null){
            return null;
        }

        List<Booking> bookingList = bookingRepository.findAllByRegisteredUser(user);
        List<BookingData> bookingDataList = new ArrayList<>();
        for (Booking booking : bookingList){

            BookingData bookingData = new BookingData();
            List<Ticket> ticketList = booking.getTicketList();
            Ticket ticket = ticketList.get(0);
            Passenger passenger = ticket.getPassenger();

            FlightData flightData = new FlightData("SG320", booking.getOrigin(), booking.getDestination(), booking.getFlightDate(), booking.getDeparture(), booking.getArrival());
            PassengerData passengerData = new PassengerData(passenger.getName(), passenger.getDob(), Luggage.valueOf(ticket.getBaggage()), Meal.valueOf(ticket.getMeal()), FClass.valueOf(ticket.getFClass()));

            bookingData.setPassengerData(passengerData);
            bookingData.setFlightData(flightData);
            bookingData.setPrice(ticket.getPrice());
            bookingData.setSeat(ticket.getSeatNumber());
            bookingData.setBookingId(booking.getId());

            bookingDataList.add(bookingData);
        }
        return bookingDataList;

    }

    public boolean deleteBookingById(int bookingId){

        Booking booking = bookingRepository.findOne(bookingId);
        if(booking == null)
            return false;
        Ticket ticket = booking.getTicketList().get(0);
        Passenger passenger = ticket.getPassenger();

        ticketRepository.delete(ticket);
        bookingRepository.delete(booking);
        passengerRepository.delete(passenger);

        return true;
    }

    public boolean updateBookingPassengerDetails(int bookingId, String passengerName, String passengerDob){
        Booking booking = bookingRepository.findOne(bookingId);
        if(booking == null)
            return false;
        Passenger passenger = booking.getTicketList().get(0).getPassenger();
        passenger.setDob(passengerDob);
        passenger.setName(passengerName);
        passengerRepository.save(passenger);
        return true;

    }

}
