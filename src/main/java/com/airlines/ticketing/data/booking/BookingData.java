package com.airlines.ticketing.data.booking;

import com.airlines.ticketing.data.flight.FlightData;
import com.airlines.ticketing.data.passenger.PassengerData;

/**
 * Created by bishal on 9/10/17.
 */
public class BookingData {

    private int bookingId;
    private PassengerData passengerData;
    private FlightData flightData;
    private String seat;
    private double price;

    public BookingData(){}

    public BookingData(int bookingId){
        this.bookingId = bookingId;
    }

    public PassengerData getPassengerData() {
        return passengerData;
    }

    public void setPassengerData(PassengerData passengerData) {
        this.passengerData = passengerData;
    }

    public FlightData getFlightData() {
        return flightData;
    }

    public void setFlightData(FlightData flightData) {
        this.flightData = flightData;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }
}
