package com.airlines.ticketing.data.search;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bishal on 9/9/17.
 */
public class SearchData {
    private String fromCity;
    private String toCity;
    private String date;
    private String numPassengers;

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNumPassengers() {
        return numPassengers;
    }

    public void setNumPassengers(String numPassengers) {
        this.numPassengers = numPassengers;
    }

    public List<String> getPurchaseLinks(){

        // Return dummy flights
        ArrayList<String> purchaseLinks = new ArrayList<String>(){
            {
                add("/purchase-ticket?flight=AG1020SX&from="+ getFromCity() +"&to=" + getToCity() + "&date=" + getDate() + "&num=" + getNumPassengers());
                add("/purchase-ticket?flight=AG1320SX&from="+ getFromCity() +"&to=" + getToCity() + "&date=" + getDate() + "&num=" + getNumPassengers());
                add("/purchase-ticket?flight=AG1620SX&from="+ getFromCity() +"&to=" + getToCity() + "&date=" + getDate() + "&num=" + getNumPassengers());

            }
        };

        return purchaseLinks;
    }
}
