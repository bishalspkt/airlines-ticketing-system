package com.airlines.ticketing.data.passenger;

public enum Luggage {
    SMALL,
    LARGE;
}
