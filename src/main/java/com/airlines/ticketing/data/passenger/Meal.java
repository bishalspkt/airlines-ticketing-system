package com.airlines.ticketing.data.passenger;

public enum Meal {
    NO_MEAL,
    SMALL_MEAL,
    LARGE_MEAL;
}
