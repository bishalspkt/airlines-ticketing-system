package com.airlines.ticketing.data.passenger;

/**
 * Created by bishal on 9/10/17.
 */




public class PassengerData {

    private String name;
    private String dob;
    private Luggage luggage;
    private Meal meal;
    private FClass fClass;

    public PassengerData() {
    }

    public PassengerData(String name, String dob, Luggage luggage, Meal meal, FClass fClass) {
        this.name = name;
        this.dob = dob;
        this.luggage = luggage;
        this.meal = meal;
        this.fClass = fClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Luggage getLuggage() {
        return luggage;
    }

    public void setLuggage(Luggage luggage) {
        this.luggage = luggage;
    }

    public Meal getMeal() {return meal;}

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public FClass getFClass() { return fClass; }

    public void setFClass(FClass fClass) { this.fClass = fClass; }
}
