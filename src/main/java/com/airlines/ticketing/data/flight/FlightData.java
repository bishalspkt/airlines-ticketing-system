package com.airlines.ticketing.data.flight;

/**
 * Created by bishal on 9/10/17.
 */
public class FlightData {

    private String name;
    private String fromCity;
    private String toCity;
    private String date;
    private String departure;
    private String arrival;

    public FlightData(String name, String fromCity, String toCity, String date, String departure, String arrival) {
        this.name = name;
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.date = date;
        this.departure = departure;
        this.arrival = arrival;
    }

    public String getName(){
        return name;
    }

    public void setName(){
        this.name = name;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }
}
