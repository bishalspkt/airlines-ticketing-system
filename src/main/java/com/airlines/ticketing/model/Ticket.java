package com.airlines.ticketing.model;

import javax.persistence.*;

/**
 * Created by bishal on 9/9/17.
 */
@Entity
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String meal;

    private String fClass;

    private String baggage;

    private String seatNumber;

    private String flightName;

    private double price;

    @ManyToOne
    private Booking booking;

    @OneToOne
    private Passenger passenger;

    public Ticket(Booking booking, Passenger passenger, String meal, String baggage, String fClass, String seatNumber, String flightName, double price) {
        this.booking = booking;
        this.passenger = passenger;
        this.meal = meal;
        this.baggage = baggage;
        this.seatNumber = seatNumber;
        this.flightName = flightName;
        this.price = price;
        this.fClass = fClass;
    }

    public Ticket(){}

    public int getId() {
        return id;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public String getFClass() { return fClass; }

    public void setFClass(String fClass) { this.fClass = fClass; }

    public String getBaggage() {
        return baggage;
    }

    public void setBaggage(String baggage) {
        this.baggage = baggage;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

}
