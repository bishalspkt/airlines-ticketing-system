package com.airlines.ticketing.model;

import javax.persistence.*;

/**
 * Created by bishal on 9/9/17.
 */
@Entity
public class Passenger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String dob;

    public Passenger(){}

    public Passenger(String name, String dob) {

        this.name = name;
        this.dob = dob;
    }
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

}
