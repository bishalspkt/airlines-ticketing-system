package com.airlines.ticketing.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by bishal on 9/10/17.
 */
@Entity
public class RegisteredUser extends User{

    private String password;

    @OneToMany(targetEntity = Booking.class, fetch = FetchType.LAZY)
    private List<Booking> bookingList;

    public RegisteredUser() {
    }

    public RegisteredUser(String name, String email, String password) {
        super(name, email);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
