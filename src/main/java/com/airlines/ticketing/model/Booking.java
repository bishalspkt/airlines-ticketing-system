package com.airlines.ticketing.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bishal on 9/9/17.
 */

@Entity
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String origin;
    private String destination;
    private String flightDate;
    private String departure;
    private String arrival;

    @ManyToOne
    private RegisteredUser registeredUser;

    @OneToMany(targetEntity = Ticket.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="booking_id")
    private List<Ticket> ticketList = new ArrayList<>();

    public Booking(RegisteredUser user, String origin, String destination, String flightDate, String departure, String arrival) {
        this.registeredUser = user;
        this.origin = origin;
        this.destination = destination;
        this.flightDate = flightDate;
        this.departure = departure;
        this.arrival = arrival;
    }

    public Booking(){}

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(String flightDate) {
        this.flightDate = flightDate;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public List<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }

    public int getId(){return id;}

}
