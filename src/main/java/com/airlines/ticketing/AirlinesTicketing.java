package com.airlines.ticketing;

import com.airlines.ticketing.model.Booking;
import com.airlines.ticketing.repository.booking.BookingRepository;
import com.airlines.ticketing.service.AuthenticationService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Created by bishal on 9/9/17.
 */

@SpringBootApplication
public class AirlinesTicketing {

    public static void main(String[] args) {
        SpringApplication.run(AirlinesTicketing.class, args);
    }

    @Bean
    CommandLineRunner runner(AuthenticationService authenticationService){
        return args -> {
            authenticationService.addUser();
        };
    }
}