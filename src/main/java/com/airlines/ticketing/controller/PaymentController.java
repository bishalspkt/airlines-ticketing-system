package com.airlines.ticketing.controller;

import com.airlines.ticketing.data.booking.BookingData;
import com.airlines.ticketing.data.flight.FlightData;
import com.airlines.ticketing.data.passenger.PassengerData;
import com.airlines.ticketing.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * Created by bishal on 9/10/17.
 */
@Controller
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    BookingService bookingService;


    @RequestMapping("/")
    public String payment(){
        return "redirect:/payment/success";
    }

    @RequestMapping("/success")
    public String paymentSuccess(HttpSession httpSession, Model model){
        // This is where we call booking servie with the data

        String seat = (String)httpSession.getAttribute("seat");
        PassengerData passengerData = (PassengerData) httpSession.getAttribute("passengerData");
        String contactName = (String) httpSession.getAttribute("name");
        String contactEmail = (String) httpSession.getAttribute("email");
        System.out.print("PaymentController : Email - " + contactEmail);
        FlightData flightData = (FlightData) httpSession.getAttribute("flightData");
        double price = (double) httpSession.getAttribute("price");

        /* Data Gathered so far */

        BookingData bookingData = bookingService.AddBooking(passengerData, flightData, contactName, contactEmail, seat, price);

        return "redirect:/ticket-dispatch?booking_number=" + bookingData.getBookingId();
    }
}
