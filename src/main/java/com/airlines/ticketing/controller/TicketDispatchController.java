package com.airlines.ticketing.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Created by bishal on 9/10/17.
 */

@Controller
@RequestMapping("/ticket-dispatch")
public class TicketDispatchController {

    @GetMapping
    public String dispatchTicket(@RequestParam("booking_number") int bookingId,
                                 HttpSession httpSession, Model model){

        model.addAttribute("bookingId", bookingId);
        return "ticketdispatch/show-ticket";
    }
}
