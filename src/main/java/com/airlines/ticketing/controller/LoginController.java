package com.airlines.ticketing.controller;

import com.airlines.ticketing.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * Created by bishal on 9/9/17.
 */

@Controller
public class LoginController {

    @Autowired
    AuthenticationService authenticationService;

    @GetMapping("/")
    public  String index(){
        return "login/index";

    }

    @GetMapping("/login")
    public String login(HttpSession httpSession){
        if (httpSession.getAttribute("logged_in") != null)
        {
            return "redirect:/flight-search/";
        }
        return "login/login";

    }

    @PostMapping("/login")
    public String loginAttempt(HttpSession httpSession, @RequestParam("email") String email, @RequestParam("password") String password){

        if (authenticationService.authenticate(email, password)){
            httpSession.setAttribute("logged_in", true);
            httpSession.setAttribute("email", email);
            httpSession.setAttribute("name", authenticationService.getName(email));

            return "redirect:/flight-search/";
        }
        return "login/login";

    }
}
