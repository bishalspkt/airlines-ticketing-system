package com.airlines.ticketing.controller;

import com.airlines.ticketing.data.flight.FlightData;
import com.airlines.ticketing.data.passenger.PassengerData;
import com.airlines.ticketing.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * Created by bishal on 9/9/17.
 */

@Controller
@RequestMapping("/purchase-ticket")
public class PurchaseTicketController {

    @RequestMapping(method = RequestMethod.GET)
    public String index(@RequestParam("flight") String flight,
                        @RequestParam("from") String from,
                        @RequestParam("to") String to,
                        @RequestParam("date") String date,
                        @RequestParam("num") String num,
                        Model model,
                        HttpSession httpSession){

        FlightData flightData = new FlightData(flight, from, to, date, "10:00 AM", "11:10 AM");

        /* Remove all session attributes */

        httpSession.removeAttribute("flightData");
        httpSession.removeAttribute("numPassengers");
        httpSession.removeAttribute("passenger");

        /* End remove all session attribute */

        httpSession.setAttribute("flightData", flightData);
        httpSession.setAttribute("numPassengers", Integer.parseInt(num));

        /* If logged in, dont ask for contact details */

        if( httpSession.getAttribute("logged_in") != null){
            model.addAttribute("passengerData", new PassengerData());

            return "purchaseticket/passenger-details";
        }

        model.addAttribute("flight", flightData);
        return "purchaseticket/contact-details";
    }

    @PostMapping("/2")
    public String bookingPassengerForm(HttpSession httpSession,
                             @RequestParam("name") String name,
                             @RequestParam("email") String email,
                             Model model){

        httpSession.setAttribute("name", name);
        httpSession.setAttribute("email", email);

        model.addAttribute("passengerData", new PassengerData());

        return "purchaseticket/passenger-details";
    }

    @PostMapping("/3")
    public String bookingSeatForm(@ModelAttribute PassengerData passengerData, HttpSession httpSession, Model model){
        httpSession.setAttribute("passengerData", passengerData);
        return "purchaseticket/seat-details";
    }

    @PostMapping("/4")
    public String bookingConfirmation(HttpSession httpSession,
                            @RequestParam("seat") String seat,
                            Model model){

        httpSession.setAttribute("seat", seat);

        /* Calculate the price */
        double price = 346.20;
        httpSession.setAttribute("price", price);

        PassengerData passengerData = (PassengerData) httpSession.getAttribute("passengerData");
        String contactName = (String) httpSession.getAttribute("name");
        String contactEmail = (String) httpSession.getAttribute("email");
        FlightData flightData = (FlightData) httpSession.getAttribute("flightData");

        model.addAttribute("flightData", flightData);
        model.addAttribute("passengerData", passengerData);
        model.addAttribute("seat", seat);
        model.addAttribute("name", contactName);
        model.addAttribute("email", contactEmail);
        model.addAttribute("seat", seat);
        model.addAttribute("price", price);

        return "purchaseticket/confirm-details";
    }
}
