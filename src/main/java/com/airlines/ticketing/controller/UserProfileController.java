package com.airlines.ticketing.controller;

import com.airlines.ticketing.data.booking.BookingData;
import com.airlines.ticketing.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by bishal on 9/9/17.
 */

@Controller
@RequestMapping("/user-profile")
public class UserProfileController {

    @Autowired
    BookingService bookingService;


    @RequestMapping
    public String getBookingList(HttpSession httpSession, Model model){
        if(httpSession.getAttribute("logged_in") == null){
            // If not logged in, redirect to root
            return "redirect:/";
        }

        String email = (String) httpSession.getAttribute("email");
        List<BookingData> bookingList = bookingService.getAllBooking(email);

        System.out.println("UserProfileController:35 " + bookingList);

        model.addAttribute("bookingDataList", bookingList);
        return "userprofile/list-bookings";
    }

    @RequestMapping(params = "success")
    public String getBookingListWithMessage(@RequestParam("success") String success, HttpSession httpSession, Model model){
        if(httpSession.getAttribute("logged_in") == null){
            // If not logged in, redirect to root
            return "redirect:/";
        }

        String email = (String) httpSession.getAttribute("email");
        List<BookingData> bookingList = bookingService.getAllBooking(email);

        System.out.println("UserProfileController:35 " + bookingList);

        model.addAttribute("message", success + " successful.");
        model.addAttribute("bookingDataList", bookingList);
        return "userprofile/list-bookings";
    }

    @RequestMapping("/delete/{id}")
    public String deleteBooking(@PathVariable("id")int bookingId){
        bookingService.deleteBookingById(bookingId);
        return "redirect:/user-profile?success=Delete";
    }
    @GetMapping("/update/{id}")
    public String updateBooking(@PathVariable("id")int bookingId, Model model){
        model.addAttribute("bookingId", bookingId);
        return "userprofile/update-booking";
    }
    @PostMapping("/update/{id}")
    public String updateBooking(@PathVariable("id")int bookingId,
                                @RequestParam("name") String name,
                                @RequestParam("dob") String dob){
        bookingService.updateBookingPassengerDetails(bookingId, name, dob);
        return "redirect:/user-profile?success=Update";
    }
}
