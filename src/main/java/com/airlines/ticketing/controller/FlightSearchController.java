package com.airlines.ticketing.controller;

import com.airlines.ticketing.data.search.SearchData;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by bishal on 9/9/17.
 */
@Controller
@RequestMapping("/flight-search")
public class FlightSearchController {

    @GetMapping
    public String index(Model model){
        model.addAttribute("searchData", new SearchData());
        return "flightsearch/index";

    }

    @PostMapping("/")
    public String search(@ModelAttribute SearchData search, Model model){

        System.out.println(search.getFromCity());
        model.addAttribute("search", search);
        return "flightsearch/result";
    }
}
