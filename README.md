# Airlines Ticketing System

An Enterprise system which manges sales of tickets of a fictional airliner. 

## Users

* Anonymous Users
* Logged in Users
* Internal Sales Representatives
* Airlines Employees who administrate the system
* External partners querying and selling tickets

## Key Features

* Login
* Sign Up
* Search for flights
* View flights
* Buy ticket
* Dispense Ticket
* API
* User Profile
* Admin Views
* CRUD Flights

### Developers and maintainers

* Bishal Sapkota 
* Leon Drygala

### Shared with subject coordinators

* Dr Alex Lopez Lorca
* Dr Rachel Burrows


### Directore Tree

    src
    ├── main
    │   ├── java
    │   │   └── com
    │   │       └── airlines
    │   │           └── ticketing
    │   │               ├── AirlinesTicketing.java
    │   │               ├── controller
    │   │               │   ├── FlightSearchController.java
    │   │               │   ├── LoginController.java
    │   │               │   ├── PaymentController.java
    │   │               │   ├── PurchaseTicketController.java
    │   │               │   ├── TicketDispatchController.java
    │   │               │   └── UserProfileController.java
    │   │               ├── data
    │   │               │   ├── booking
    │   │               │   │   └── BookingData.java
    │   │               │   ├── flight
    │   │               │   │   └── FlightData.java
    │   │               │   ├── passenger
    │   │               │   │   ├── Luggage.java
    │   │               │   │   ├── Meal.java
    │   │               │   │   └── PassengerData.java
    │   │               │   └── search
    │   │               │       └── SearchData.java
    │   │               ├── model
    │   │               │   ├── Booking.java
    │   │               │   ├── Passenger.java
    │   │               │   ├── RegisteredUser.java
    │   │               │   ├── Ticket.java
    │   │               │   └── User.java
    │   │               ├── repository
    │   │               │   ├── booking
    │   │               │   │   ├── BookingRepository.java
    │   │               │   │   ├── PassengerRepository.java
    │   │               │   │   └── TicketRepository.java
    │   │               │   └── user
    │   │               │       ├── RegisteredUserRepository.java
    │   │               │       └── UserRepository.java
    │   │               └── service
    │   │                   ├── AuthenticationService.java
    │   │                   └── BookingService.java
    │   └── resources
    │       ├── application.properties
    │       └── templates
    │           ├── flightsearch
    │           │   ├── index.html
    │           │   └── result.html
    │           ├── fragments
    │           │   └── header.html
    │           ├── login
    │           │   ├── index.html
    │           │   └── login.html
    │           ├── purchaseticket
    │           │   ├── confirm-details.html
    │           │   ├── contact-details.html
    │           │   ├── passenger-details.html
    │           │   └── seat-details.html
    │           ├── ticketdispatch
    │           │   └── show-ticket.html
    │           └── userprofile
    │               ├── list-bookings.html
    │               └── update-booking.html
    └── test
        └── java
